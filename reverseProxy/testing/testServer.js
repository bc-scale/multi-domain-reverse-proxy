

const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');




exports.setupHttp = function(host='localhost',port=80){
	const apphttp = express();
	apphttp.set('port', process.env.PORT || port);
	apphttp.set('host', process.env.HOST || host);
	
	apphttp.all('*',(req,res)=>{
		res.send('Http server listening on port ' + apphttp.get('host') + ':' + apphttp.get('port'));
	});
	
	http.createServer(apphttp).listen(apphttp.get('port'), apphttp.get('host'), function(){
		console.log('Express server listening on port ' + apphttp.get('host') + ':' + apphttp.get('port'));
	});
};

exports.setupHttps = function(host='localhost',port=443){
	const apphttps = express();
	apphttps.set('port', process.env.PORT || port);
	apphttps.set('host', process.env.HOST || host);
	
	apphttps.all('*',(req,res)=>{
		res.send('Https server working');
		//res.send('Express server listening on port ' + apphttps.get('host') + ':' + apphttps.get('port'));
	});

	https.createServer({
		key: fs.readFileSync('./testing/certs/key.key'),
		cert: fs.readFileSync('./testing/certs/cert.crt'),
		passphrase: 'localhost',
	}, apphttps).listen(8001, apphttps.get('host'), function(){
		console.log('Express server listening on port ' + apphttps.get('host') + ':' + apphttps.get('port'));
	});
};